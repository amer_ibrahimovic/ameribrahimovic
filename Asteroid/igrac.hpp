#pragma once

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QMediaPlayer>
#include <pravacrakete.hpp>

class Player:public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Player();
    void keyPressEvent(QKeyEvent *event);
public slots:
    void spawn();
private:
    QMediaPlayer *releaseSound_;
    double rotationAngle_{270};
    QPoint topCenter_;
    QPoint topRight_;
    QPoint topLeft_;
    QPoint left_;
    QPoint right_;
    QPoint bottomCenter_;
    QPoint bottomRight_;
    QPoint bottomLeft_;

    void releaseProjectile(const ProjectileDirection& direction);
    void rotateRight();
    void rotateLeft();

};

