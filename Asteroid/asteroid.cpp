#include <Asteroid.hpp>
#include <QTimer>
#include <QGraphicsScene>
#include <QDebug>
#include <Scena.hpp>

extern MainScene *mainScene;

Asteroid::Asteroid(QGraphicsItem *parent):QObject(), QGraphicsPixmapItem(parent)
{
    crashSound_ = new QMediaPlayer();
    crashSound_->setMedia(QUrl("qrc:/sounds/sounds/Crash.mp3"));

    int height=rand()%550;
    int width=rand()%700;
    int side=rand()%4;
    if(side == 0){
        setPos(width, 0);
    }else if(side == 1){
        setPos(0, height);
    }else if(side == 2){
        setPos(700, height);
    }else if(side == 3){
        setPos(width, 550);
    }

    int random_number=rand()%30;
    updateAngle(45+random_number);

    setPixmap(QPixmap(":/images/images/asteroid.png"));
    setTransformOriginPoint(50,50);
    setRotation(180);
    QTimer *timer=new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));
    timer->start(50);
}

void Asteroid::move()
{
    if(collidedWithPlayer()){
        handleCollision();
        delete this;
        return;
    }

    reposition();

    if(outOfSceneBounds()){
        if(outOfYMaxBounds()){
            direction_ = ProjectileDirection::Up;
            setPos(x(), y()-10);
        } else if(outOfYMinBounds()){
            setPos(x(), y()+10);
            direction_ = ProjectileDirection::Down;
        } else if(outOfXMinBounds()){
            setPos(x()+10, y());
            direction_ = ProjectileDirection::Up;
        } else if(outOfXMaxBounds()){
            setPos(x()-10, y());
            direction_ = ProjectileDirection::Down;
        }
        int random_number=rand()%30;
        updateAngle(180+random_number);
    }
}

bool Asteroid::collidedWithPlayer(){
    QList<QGraphicsItem *> colliding_items=collidingItems();
    for(int i = 0, n = colliding_items.size(); i < n; i++){
        if(colliding_items[i]){
            if(typeid(*(colliding_items[i])) == typeid(Player)){
                return true;
            }
        }
    }
    return false;
}

void Asteroid::handleCollision(){
    crashSound_->play();
    if(!mainScene->gameOver){
        mainScene->life->decreaseNumberOfLives();
        if(mainScene->life->getNumberOfLives() == 0){
            mainScene->gameOver = true;
            mainScene->writeGameOver();
        }
    }
}

void Asteroid::updateAngle(const double& angle){
    rotationAngle_ = rotationAngle_ + angle;
    if(rotationAngle_ == 360){
        rotationAngle_ = 0;
    } else if(rotationAngle_ > 360){
        rotationAngle_ = rotationAngle_ - 360;
    }
}

void Asteroid::reposition() {
    double newX{0};
    double newY{0};
    if(direction_ == ProjectileDirection::Up){
        newX = x()-cos(rotationAngle_*(M_PI/180))*5;
        newY = y()-sin(rotationAngle_*(M_PI/180))*5;
    } else if(direction_ == ProjectileDirection::Down){
        newX = x()+cos(rotationAngle_*(M_PI/180))*5;
        newY = y()+sin(rotationAngle_*(M_PI/180))*5;
    }

    setPos(newX, newY);
}

bool Asteroid::outOfSceneBounds(){
    return outOfYMaxBounds() || outOfYMinBounds() || outOfXMaxBounds() || outOfXMinBounds();
}

bool Asteroid::outOfYMaxBounds() {
    return (pos().y() + 10) > 550;
}
bool Asteroid::outOfYMinBounds() {
    return (pos().y() - 10) < 0;
}
bool Asteroid::outOfXMaxBounds() {
    return (pos().x() + 10) > 750;
}
bool Asteroid::outOfXMinBounds() {
    return (pos().x() - 10) < 0;
}

