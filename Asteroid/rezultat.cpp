#include <QFont>
#include <rezultat.hpp>

Score::Score(QGraphicsItem *parent): QGraphicsTextItem(parent)
{
    setPlainText(QString("Score: ")+QString::number(score));
    setDefaultTextColor(Qt::red);
    setFont(QFont("times",24));
}


void Score::increase()
{
    score++;
    setPlainText(QString("Score: ")+QString::number(score));
}

int Score::getScore()
{
    return score;
}
