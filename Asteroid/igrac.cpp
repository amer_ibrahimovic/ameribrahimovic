#include <QKeyEvent>
#include <QGraphicsScene>
#include <igrac.hpp>
#include <raketa.hpp>
#include <Asteroid.hpp>

Player::Player():QObject(),QGraphicsPixmapItem()
{
    releaseSound_ = new QMediaPlayer();
    releaseSound_->setMedia(QUrl("qrc:/sounds/sounds/Gun_Powder.mp3"));

    QPixmap spaceShip{":/images/images/spaceShip.png"};
    setPixmap(spaceShip.scaled(60, 90));
}

void Player::keyPressEvent(QKeyEvent *event)
{
   if(event->key()==Qt::Key_Right){
        rotateRight();
   } else if(event->key()==Qt::Key_Left){
        rotateLeft();
   } else if(event->key()==Qt::Key_Up){
        releaseProjectile(ProjectileDirection::Up);
   } else if(event->key()==Qt::Key_Down){
        releaseProjectile(ProjectileDirection::Down);
   }
}

void Player::rotateRight(){
    setTransformOriginPoint(boundingRect().center());
    rotationAngle_ = rotationAngle_ + 10.0;
    if(rotationAngle_ == 360.0){
        rotationAngle_ = 0.0;
    }else if(rotationAngle_ > 360.0){
        rotationAngle_ = rotationAngle_ - 360.0;
    }
    setRotation(rotation() + 10);
}

void Player::rotateLeft(){
    setTransformOriginPoint(boundingRect().center());
    rotationAngle_ = rotationAngle_ - 10;
    if(rotationAngle_< 0){
        rotationAngle_ = rotationAngle_ + 360;
    }
    setRotation(rotation() - 10);
}

void Player::releaseProjectile(const ProjectileDirection& direction){

    releaseSound_->play();
    Projectile *projectile=new Projectile();
    projectile->setAngle(rotationAngle_);
    projectile->setDirection(direction);
    projectile->setTransformOriginPoint(projectile->boundingRect().center());
    if(direction == ProjectileDirection::Up){
        if(rotationAngle_ > 250.0 && rotationAngle_ <= 290.0){
            projectile->setPos(x()+15,y()-30);
        } else if(rotationAngle_ >= 200.0 && rotationAngle_ <= 250.0){
            projectile->setPos(x()-30,y()-30);
        } else if(rotationAngle_ > 290.0 && rotationAngle_ <= 330.0){
            projectile->setPos(x()+45,y()-30);
        } else if(rotationAngle_ >= 160 && rotationAngle_ < 200.0){
            projectile->setPos(x()-15,y());
        } else if(rotationAngle_ > 330.0 || rotationAngle_ < 30.0){
            projectile->setPos(x()+45,y());
        } else if(rotationAngle_ >= 30.0 && rotationAngle_ <= 60.0){
            projectile->setPos(x()+45,y()+80);
        } else if(rotationAngle_ > 60.0 && rotationAngle_ <= 110.0){
            projectile->setPos(x()+15,y()+80);
        } else if(rotationAngle_ > 110.0 && rotationAngle_ < 160.0){
            projectile->setPos(x()-30,y()+80);
        }
        projectile->setRotation(rotationAngle_ + 90);
    } else {
        if(rotationAngle_ > 250.0 && rotationAngle_ <= 290.0){
            projectile->setPos(x()+15,y()+80);
        } else if(rotationAngle_ >= 200.0 && rotationAngle_ <= 250.0){
            projectile->setPos(x()+45,y()+80);
        } else if(rotationAngle_ > 290.0 && rotationAngle_ <= 330.0){
            projectile->setPos(x()-30,y()+80);
        } else if(rotationAngle_ >= 160 && rotationAngle_ < 200.0){
            projectile->setPos(x()+45,y());
        } else if(rotationAngle_ > 330.0 || rotationAngle_ < 30.0){
            projectile->setPos(x()-15,y());
        } else if(rotationAngle_ >= 30.0 && rotationAngle_ <= 60.0){
            projectile->setPos(x()-30,y()-30);
        } else if(rotationAngle_ > 60.0 && rotationAngle_ <= 110.0){
            projectile->setPos(x()+15,y()-30);
        } else if(rotationAngle_ > 110.0 && rotationAngle_ < 160.0){
            projectile->setPos(x()+45,y()-30);
        }
        projectile->setRotation(rotationAngle_ - 90);
    }
    scene()->addItem(projectile);
}


void Player::spawn()
{
    Asteroid *asteroid=new Asteroid();
    scene()->addItem(asteroid);
}

