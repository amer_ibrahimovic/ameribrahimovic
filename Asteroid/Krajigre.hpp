#pragma once

#include <QGraphicsTextItem>

class GameOver : public QGraphicsTextItem
{
public:
    GameOver(QGraphicsItem * parent=0);
    void activate();
};

