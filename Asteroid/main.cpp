#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsRectItem>
#include <QApplication>
#include <QTimer>
#include <igrac.hpp>
#include <Scena.hpp>

MainScene *mainScene;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    mainScene=new MainScene();

    mainScene->show();

    return a.exec();
}
