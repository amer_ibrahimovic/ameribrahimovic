#include <Krajigre.hpp>
#include <QFont>

GameOver::GameOver(QGraphicsItem *parent)
{

}

void GameOver::activate(){
    setPlainText(QString("GAME OVER"));
    setDefaultTextColor(Qt::red);
    setFont(QFont("times",42));
}
