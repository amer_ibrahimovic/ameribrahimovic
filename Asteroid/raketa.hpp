#pragma once

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QMediaPlayer>
#include "pravacrakete.hpp"


class Projectile:public QObject, public QGraphicsPixmapItem
{


    Q_OBJECT
public:
    Projectile(QGraphicsItem * parent=0);
    void setAngle(const double& angle);
    void setDirection(const ProjectileDirection& direction);
public slots:
    void move();
private:
    QMediaPlayer *collisionSound_;
    QMediaPlayer *leavingSceneSound_;
    double rotationAngle_;
    ProjectileDirection direction_{ProjectileDirection::Up};

    bool collidedWithAsteroid();
    void handleCollision();

    void reposition();
    bool outOfSceneBounds();
};

