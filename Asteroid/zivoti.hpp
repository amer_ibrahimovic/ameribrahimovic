#pragma once

#include <QGraphicsTextItem>

class PlayerLife : public QGraphicsTextItem
{
public:
    PlayerLife(QGraphicsItem * parent=0);
    void decreaseNumberOfLives();
    int getNumberOfLives();

private:
    int lives_{3};
};

