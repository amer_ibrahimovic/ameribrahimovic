#include <Scena.hpp>
#include <Asteroid.hpp>
#include <QTimer>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QBrush>
#include <QImage>

MainScene::MainScene(QWidget *parent)
{
    QGraphicsScene *scene=new QGraphicsScene();
    scene->setSceneRect(0,0,800,600);
    setBackgroundBrush(QBrush(QImage(":images/images/spaceBackground.jpeg").scaled(1000, 604)));

    setScene(scene);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(800,600);

    Player *player=new Player();
    player->setPos(380,250);
    player->setFlag(QGraphicsItem::ItemIsFocusable);
    player->setFocus();
    scene->addItem(player);

    score = new Score();
    scene->addItem(score);

    life = new PlayerLife();
    life->setPos(life->x(),life->y()+25);
    scene->addItem(life);

    gameOverPrint = new GameOver();
    gameOverPrint->setPos(250, 150);
    scene->addItem(gameOverPrint);

    QMediaPlaylist *playlist = new QMediaPlaylist();
    playlist->addMedia(QUrl("qrc:/sounds/sounds/Alien_Song.mp3"));
    playlist->setPlaybackMode(QMediaPlaylist::Loop);

    QMediaPlayer *music = new QMediaPlayer();
    music->setPlaylist(playlist);
    music->play();

    QTimer *spawnTimer=new QTimer();
    QObject::connect(spawnTimer,SIGNAL(timeout()),player,SLOT(spawn()));
    spawnTimer->start(2000);

    show();
}

void MainScene::writeGameOver(){
    gameOverPrint->activate();
  // scene->clear();
//  setBackgroundBrush(QBrush(QImage(":images/images/gameOver.jpeg")));
}

