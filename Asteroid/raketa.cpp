#include <typeinfo>
#include <QTimer>
#include <QDebug>
#include <QGraphicsScene>
#include <raketa.hpp>
#include <Asteroid.hpp>
#include <Scena.hpp>

extern MainScene *mainScene;

Projectile::Projectile(QGraphicsItem *parent):QObject(), QGraphicsPixmapItem(parent)
{
    QPixmap rocket{":/images/images/rocket.png"};
    setPixmap(rocket.scaled(20, 35));

    collisionSound_ =new QMediaPlayer();
    collisionSound_->setMedia(QUrl("qrc:/sounds/sounds/Big_Explosion_Cut_Off.mp3"));

    leavingSceneSound_ =new QMediaPlayer();
    leavingSceneSound_->setMedia(QUrl("qrc:/sounds/sounds/Big_Explosion_Distant.mp3"));

    QTimer *timer=new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));
    timer->start(50);
}

void Projectile::setAngle(const double& angle){
    rotationAngle_ = angle;
}

void Projectile::setDirection(const ProjectileDirection& direction) {
    direction_ = direction;
}

void Projectile::move()
{
    if(collidedWithAsteroid()){
        handleCollision();
        delete this;
        return;
    }

    reposition();

    if(outOfSceneBounds()){
        leavingSceneSound_->play();
        scene()->removeItem(this);
        delete this;
        return;
    }
}

bool Projectile::collidedWithAsteroid(){
    QList<QGraphicsItem *> colliding_items=collidingItems();
    for(int i = 0, n = colliding_items.size(); i < n; i++){
        if(colliding_items[i]){
            if(typeid(*(colliding_items[i])) == typeid(Asteroid)){
                scene()->removeItem(colliding_items[i]);
                delete colliding_items [i];
                return true;
            }
        }
    }
    return false;
}

void Projectile::handleCollision(){
    collisionSound_->play();
    if(!mainScene->gameOver){
        mainScene->score->increase();
    }
    scene()->removeItem(this);
}

void Projectile::reposition(){
    double newX{0};
    double newY{0};
    if(direction_ == ProjectileDirection::Up){
        newX = x()+cos(rotationAngle_*(M_PI / 180.0))*10;
        newY = y()+sin(rotationAngle_*(M_PI / 180.0))*10;
    } else if(direction_ == ProjectileDirection::Down){
        newX = x()-cos(rotationAngle_*(M_PI / 180.0))*10;
        newY = y()-sin(rotationAngle_*(M_PI / 180.0))*10;
    }

    setPos(newX, newY);
}

bool Projectile::outOfSceneBounds(){
    return (pos().y() + 10) > 600 || (pos().x() + 10) > 750 || (pos().y() - 10) < 0 || (pos().x() - 10) < 0;
}
