#pragma once

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QMediaPlayer>
#include <pravacrakete.hpp>

class Asteroid:public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Asteroid(QGraphicsItem * parent=0);
public slots:
    void move();


private:
    QMediaPlayer *crashSound_;
    double rotationAngle_{0};
    ProjectileDirection direction_{ProjectileDirection::Down};

    bool collidedWithPlayer();
    void handleCollision();

    void updateAngle(const double& angle);
    void reposition();

    bool outOfSceneBounds();
    bool outOfYMaxBounds();
    bool outOfYMinBounds();
    bool outOfXMaxBounds();
    bool outOfXMinBounds();
};

