#pragma once

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QWidget>
#include <igrac.hpp>
#include <rezultat.hpp>
#include <zivoti.hpp>
#include <Krajigre.hpp>


class MainScene: public QGraphicsView
{
public:
    MainScene(QWidget *parent=0);
    bool gameOver{false};
    QGraphicsScene *scene;
    Player *player;
    Score *score;
    PlayerLife *life;
    GameOver *gameOverPrint;

    void writeGameOver();
};

