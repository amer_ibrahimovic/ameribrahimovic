#-------------------------------------------------
#
# Project created by QtCreator 2019-09-28T10:19:01
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Zavrsni
TEMPLATE = app


SOURCES += main.cpp \
    Asteroid.cpp \
    Krajigre.cpp \
    Scena.cpp \
    igrac.cpp \
    zivoti.cpp \
    raketa.cpp \
    rezultat.cpp


HEADERS  += \
    Asteroid.hpp \
    Krajigre.hpp \
    Scena.hpp \
    igrac.hpp \
    zivoti.hpp \
    raketa.hpp \
    pravacrakete.hpp \
    rezultat.hpp


FORMS    +=

RESOURCES += \
    myresources.qrc

