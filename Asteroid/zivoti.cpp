#include <zivoti.hpp>
#include <QFont>

PlayerLife::PlayerLife(QGraphicsItem *parent)
{
    setPlainText(QString("Lives: ")+ QString::number(lives_));
    setDefaultTextColor(Qt::yellow);
    setFont(QFont("times",24));
}


void PlayerLife::decreaseNumberOfLives()
{
    lives_--;
    setPlainText(QString("Lives: ")+ QString::number(lives_));
}

int PlayerLife::getNumberOfLives()
{
    return lives_;
}
